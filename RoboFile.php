<?php
include('src/Yarn.php');
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see https://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
    use \RomeoBot\Yarn;
    // define public methods as commands
}