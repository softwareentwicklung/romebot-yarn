<?php

namespace RomeoBot;

trait Yarn {
    public function romeobotYarn(){
        $file = file_get_contents('package.json');
        $data = json_decode($file, true);

        exec('git reset --hard');
        exec('git checkout -b dependabot');

        if ($data['dependencies']) {
            foreach ($data['dependencies'] as $name => $version) {
                if ($name) {
                    exec('yarn add ' . $name);
                }
            }
        }

        if ($data['devDependencies']) {
            foreach ($data['devDependencies'] as $name => $version) {
                if ($name) {
                    exec('yarn add ' . $name . ' --dev');
                }
            }
        }

        exec('git diff --quiet && git diff --staged --quiet || git commit -am "Depenabot update from '.date('d-m-Y').'"; git push origin dependabot -u');
        exec('git reset --hard');
        exec('git checkout main');
    }
}